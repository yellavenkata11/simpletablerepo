import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"],
})
export class AppComponent {
    title = "simple-table";
    private _jsonURL = "assets/data.json";
    data: any;

    constructor(private http: HttpClient) {
        this.getJSON().subscribe((data) => {
            this.data = data;
            console.log(this.data);
        });
    }

    public getJSON(): Observable<any> {
        return this.http.get(this._jsonURL);
    }
}
